
package com.paulsdevblog.exceptions;

/**
 * Simple Exception extension for logging and re-throwing purposes.
 *
 * @author PaulsDevBlog.com
 */
public class ServiceLayerDataException extends Exception {
    
    private String serviceCauseMethod;
            
    public ServiceLayerDataException( String message, String serviceCauseMethod ) {
        super(message);
        this.serviceCauseMethod = serviceCauseMethod;
    }
    
    public String getServiceCauseMethod() {
        return serviceCauseMethod;
    }

    public void setServiceCauseMethod(String serviceCauseMethod) {
        this.serviceCauseMethod = serviceCauseMethod;
    }
    
    
    
}
