
package com.paulsdevblog.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configuration class for Swagger SpringFox
 *
 * @author PaulsDevBlog.com
 */
@Component
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    
    private static final Logger logger = LoggerFactory.getLogger( SwaggerConfig.class );
    
    /** The title for the spring boot service to be displayed on swagger UI.  */  
    @Value("${swagger.title:spring.application.name:app_title}")  
    private String title;  
    
    /** The description of the spring boot service. */  
    @Value("${swagger.description:spring.application.description:app_description}")  
    private String description;  
   
    /** The version of the service. */  
    @Value("${swagger.version:spring.application.version:versionxxx}")  
    private String version;  
   
    /** The terms of service url for the service if applicable. */  
    @Value("${swagger.termsOfServiceUrl:terms_of_service_url:}")  
    private String termsOfServiceUrl;  
   
    /** The contact name for the service. */  
    @Value("${swagger.contact.name:contact_name}")  
    private String contactName;  
   
    /** The contact url for the service. */  
    @Value("${swagger.contact.url:contact_url}")  
    private String contactURL;  
   
    /** The contact email for the service. */  
    @Value("${swagger.contact.email:email_address}")  
    private String contactEmail;  
   
    /** The license for the service if applicable. */  
    @Value("${swagger.license:license_body}")  
    private String license;  
   
    /** The license url for the service if applicable. */  
    @Value("${swagger.licenseUrl:client_licenseUrl}")  
    private String licenseURL;  

    
    /**  
    * This method will return the API info object to swagger which will in turn 
    * display the information on the swagger UI.  
    * 
    * See the active application.properties file for adjusting attributes.
    *  
    * @return the API information object
    */ 
    private ApiInfo apiEndPointsInfo() {
        
        return new ApiInfoBuilder()
                    .title( this.title )
                    .description( this.description )
                    .contact( new Contact(this.contactName, this.contactURL, this.contactEmail ))
                    .license( this.license )
                    .licenseUrl( this.licenseURL )
                    .version( this.version )
                    .build();

    }
    
    /**  
     *  Swagger API Endpoint for Customer
     * 
     * This method will return the Docket API object to swagger for the 
     * this micro-service application defined end-points, which will in turn 
     * display the information on the swagger UI.  
     *  
     * Refer the URL http://{ip-address or host-name}:{service.port}/{server.contextPath}/swagger-ui.html  
     * 
     * @return the docket object
     */ 
    @Bean
    public Docket docketAPICustomer() {
        
        Docket docket = new Docket( DocumentationType.SWAGGER_2 )
            .groupName( "APICustomer" )
            .select()
            .apis( RequestHandlerSelectors.basePackage("com.paulsdevblog.springboothibernaterelationships.controller") )
            .paths(PathSelectors.ant( "/customer/**"))
            .build()
            .apiInfo( apiEndPointsInfo() );
        
        String logging_message = "Configured SWAGGER Group: [" + String.valueOf( docket.getGroupName() ) + "]" ;
        logger.info( logging_message );
        
        return docket;
    }
    
    /**  
     *  Swagger API Endpoint for CustomerLocation
     * 
     * This method will return the Docket API object to swagger for the 
     * this micro-service application defined end-points, which will in turn 
     * display the information on the swagger UI.  
     *  
     * Refer the URL http://{ip-address or host-name}:{service.port}/{server.contextPath}/swagger-ui.html  
     * 
     * @return the docket object
     */ 
    @Bean
    public Docket docketAPICustomerLocation() {
        
        Docket docket = new Docket( DocumentationType.SWAGGER_2 )
            .groupName( "APICustomerLocation" )
            .select()
            .apis( RequestHandlerSelectors.basePackage("com.paulsdevblog.springboothibernaterelationships.controller") )
            .paths(PathSelectors.ant( "/customerlocation/**"))
            .build()
            .apiInfo( apiEndPointsInfo() );
        
        String logging_message = "Configured SWAGGER Group: [" + String.valueOf( docket.getGroupName() ) + "]" ;
        logger.info( logging_message );
        
        return docket;
    }
    
    /**  
     *  Swagger API Endpoint for LocationRegion
     * 
     * This method will return the Docket API object to swagger for the 
     * this micro-service application defined end-points, which will in turn 
     * display the information on the swagger UI.  
     *  
     * Refer the URL http://{ip-address or host-name}:{service.port}/{server.contextPath}/swagger-ui.html  
     * 
     * @return the docket object
     */ 
    @Bean
    public Docket docketAPILocationRegion() {
        
        Docket docket = new Docket( DocumentationType.SWAGGER_2 )
            .groupName( "APILocationRegion" )
            .select()
            .apis( RequestHandlerSelectors.basePackage("com.paulsdevblog.springboothibernaterelationships.controller") )
            .paths(PathSelectors.ant( "/locationregion/**"))
            .build()
            .apiInfo( apiEndPointsInfo() );
        
        String logging_message = "Configured SWAGGER Group: [" + String.valueOf( docket.getGroupName() ) + "]" ;
        logger.info( logging_message );
        
        return docket;
    }
    
    
    /**  
     *  Swagger API Endpoint for LocationType
     * 
     * This method will return the Docket API object to swagger for the 
     * this micro-service application defined end-points, which will in turn 
     * display the information on the swagger UI.  
     *  
     * Refer the URL http://{ip-address or host-name}:{service.port}/{server.contextPath}/swagger-ui.html  
     * 
     * @return the docket object
     */ 
    @Bean
    public Docket docketAPILocationType() {
        
        Docket docket = new Docket( DocumentationType.SWAGGER_2 )
            .groupName( "APILocationType" )
            .select()
            .apis( RequestHandlerSelectors.basePackage("com.paulsdevblog.springboothibernaterelationships.controller") )
            .paths(PathSelectors.ant( "/locationtype/**"))
            .build()
            .apiInfo( apiEndPointsInfo() );
        
        String logging_message = "Configured SWAGGER Group: [" + String.valueOf( docket.getGroupName() ) + "]" ;
        logger.info( logging_message );
        
        return docket;
    }
}
