
package com.paulsdevblog.springboothibernaterelationships.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paulsdevblog.springboothibernaterelationships.domain.model.Customer;
import com.paulsdevblog.springboothibernaterelationships.domain.repository.CustomerRepository;
import com.paulsdevblog.springboothibernaterelationships.domain.service.CustomerService;

import com.paulsdevblog.exceptions.ServiceLayerDataException;
import com.paulsdevblog.springboothibernaterelationships.domain.dto.CustomerDTO;


/**
 * Service layer implementation for 
 * Customer
 *
 * @author PaulsDevBlog.com
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    
    private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class );
    
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl( CustomerRepository customerRepository ){
        this.customerRepository = customerRepository;
    }
    
    /**
     * Find all and list Customer
     * 
     * @return List of Customer
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<Customer> findAll() throws ServiceLayerDataException {
        try {

            return this.customerRepository.findAll();

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
    /**
     * Find list of Customer matching customerUuid
     * 
     * @param customerUuid parameter UUID
     * @return list of Customer 
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<Customer> findByCustomerUuid( String customerUuid ) throws ServiceLayerDataException {
        try {

            return this.customerRepository.findByCustomerUuidAllIgnoreCaseOrderByCustomerUuidAsc(String.valueOf( customerUuid ) );

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }

    /**
     * Find list of Customer matching customerName
     * 
     * @param customerName parameter name
     * @return list of Customer 
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<Customer> findByCustomerName( String customerName ) throws ServiceLayerDataException {
        try {

            return this.customerRepository.findByCustomerNameAllIgnoreCaseOrderByCustomerNameAsc(String.valueOf( customerName ) );

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
    @Override
    public Customer getOneByCustomerUuid( String customerUuid ) throws ServiceLayerDataException{
        try {

            List<Customer> customerList =  this.customerRepository.findByCustomerUuidAllIgnoreCaseOrderByCustomerUuidAsc(String.valueOf( customerUuid ) );
            
            if ( !customerList.isEmpty() ) {
                
                return customerList.get(0);
                
            } else {
                
                return null;
                
            }

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }

    @Override
    public Customer updateCustomer( CustomerDTO customerDTO ) throws ServiceLayerDataException{
        try {

            //Get keys from DTO
            String customerUuid = String.valueOf( customerDTO.getCustomerUuid() );

            //Get current record with Ref and UUID keys or Hibernate JPA will try to set ref key to null, and update will fail
            Customer customer = this.getOneByCustomerUuid(customerUuid );
            
            if ( customer == null ) { 
                throw new ServiceLayerDataException( "Record not Found. Cannot Update.", String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() );
            } 
            
            customer.setCustomerName( customerDTO.getCustomerName());  
            
            customer.setLastModifiedUser( customerDTO.getLastModifiedUser() ); 

            //Hibernate JPA save and flush (commit) persist
            Customer returnCustomer = this.customerRepository.saveAndFlush( customer );

            //Return modified Entity
            return returnCustomer;

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
    @Override
    public Customer insertCustomer( CustomerDTO customerDTO ) throws ServiceLayerDataException{
        try {

            //Setup Entity for Hibernate JPA action
            Customer customer = new Customer();
            
            //Null keys to ensure Hibernate JPA treats this and Entity INSERT
            customer.setCustomerRef( null );
            customer.setCustomerUuid( null );
            
            customer.setCustomerName( customerDTO.getCustomerName());  
            
            customer.setLastModifiedUser( customerDTO.getLastModifiedUser() ); 

            //Hibernate JPA save and flush (commit) persist
            Customer returnCustomer = this.customerRepository.saveAndFlush( customer );

            //Return modified Entity
            return returnCustomer;

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
}
