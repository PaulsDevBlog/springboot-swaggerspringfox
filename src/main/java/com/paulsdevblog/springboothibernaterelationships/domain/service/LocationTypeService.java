
package com.paulsdevblog.springboothibernaterelationships.domain.service;

import java.util.List;

import com.paulsdevblog.springboothibernaterelationships.domain.model.LocationType;
import com.paulsdevblog.springboothibernaterelationships.domain.repository.LocationTypeRepository;
import com.paulsdevblog.exceptions.ServiceLayerDataException;

/**
 * Service layer interface for 
 * LocationType
 *
 * @author PaulsDevBlog.com
 */
public interface LocationTypeService {
    
    public List<LocationType> findAll() throws ServiceLayerDataException;
    
    public List<LocationType> findByLocationTypeUuid( String locationTypeUuid ) throws ServiceLayerDataException;
    public List<LocationType> findByLocationTypeCode( String locationTypeCode ) throws ServiceLayerDataException;
    
}
