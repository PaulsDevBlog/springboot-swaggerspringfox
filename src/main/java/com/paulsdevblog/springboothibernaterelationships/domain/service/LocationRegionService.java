
package com.paulsdevblog.springboothibernaterelationships.domain.service;

import java.util.List;

import com.paulsdevblog.springboothibernaterelationships.domain.model.LocationRegion;
import com.paulsdevblog.springboothibernaterelationships.domain.repository.LocationRegionRepository;
import com.paulsdevblog.exceptions.ServiceLayerDataException;

/**
 * Service layer interface for 
 * LocationRegion
 *
 * @author PaulsDevBlog.com
 */
public interface LocationRegionService {
    
    public List<LocationRegion> findAll() throws ServiceLayerDataException;
    
    public List<LocationRegion> findByLocationRegionUuid( String locationRegionUuid ) throws ServiceLayerDataException;
    public List<LocationRegion> findByLocationRegionCode( String locationRegionCode ) throws ServiceLayerDataException;
    
}
