package com.paulsdevblog.springboothibernaterelationships.domain.views;

/**
 * Simple interface illustrating use of JSONViews
 *
 * @author paulsdevblogl.com
 */
public interface EntityViews {

    /**
     * JSONViews identifier label for entity attributes that are to be
     * included in simple lists of entities
     */
    public static interface List {}
    
    /**
     * JSONViews identifier label for entity details, and inherits any
     * entities annotated with EntityViews.List
     */
    public static interface Details extends List {}
    
    /**
     * JSONViews identifier label for entity and parent relationships, and inherits any
     * entities annotated with EntityViews.List and EntityViews.Details
     */
    public static interface EntityParents extends Details {}
    
    /**
     * JSONViews identifier label for entity and child relationships, and inherits any
     * entities annotated with EntityViews.List and EntityViews.Details
     */
    public static interface EntityChildren extends Details {}


}
