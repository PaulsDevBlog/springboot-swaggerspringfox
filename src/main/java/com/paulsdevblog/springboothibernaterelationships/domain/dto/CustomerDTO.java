
package com.paulsdevblog.springboothibernaterelationships.domain.dto;

import java.util.Objects;

/**
 * Basic controller method data transfer object (DTO) for 
 * customer
 *
 * @author PaulsDevBlog.com
 */
public class CustomerDTO {
    
    private String customerUuid;
    private String customerName;
    private String lastModifiedUser;

    public CustomerDTO() {}

    public CustomerDTO(
        String customerUuid, 
        String customerName, 
        String lastModifiedUser
    ) {
        this.customerUuid = customerUuid;
        this.customerName = customerName;
        this.lastModifiedUser = lastModifiedUser;
    }

    public String getCustomerUuid() {
        return customerUuid;
    }

    public void setCustomerUuid(String customerUuid) {
        this.customerUuid = customerUuid;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(String lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.customerUuid);
        hash = 53 * hash + Objects.hashCode(this.customerName);
        hash = 53 * hash + Objects.hashCode(this.lastModifiedUser);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CustomerDTO other = (CustomerDTO) obj;
        if (!Objects.equals(this.customerUuid, other.customerUuid)) {
            return false;
        }
        if (!Objects.equals(this.customerName, other.customerName)) {
            return false;
        }
        if (!Objects.equals(this.lastModifiedUser, other.lastModifiedUser)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CustomerDTO{" 
                    + "customerUuid=" + customerUuid 
                    + ", customerName=" + customerName 
                    + ", lastModifiedUser=" + lastModifiedUser 
                + '}';
    }
    
    
    
    
    
    
}
