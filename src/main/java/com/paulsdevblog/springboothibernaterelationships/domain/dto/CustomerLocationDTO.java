
package com.paulsdevblog.springboothibernaterelationships.domain.dto;

import java.util.Objects;

/**
 * Basic controller method data transfer object (DTO) for 
 * CustomerLocation
 *
 * @author PaulsDevBlog.com
 */
public class CustomerLocationDTO {

    
    private String customerLocationUuid;
    private String customerLocationCode;
                            
    private String customerUuid;
    private String locationTypeCode;
    private String locationRegionCode;
    
    private String lastModifiedUser;

    public CustomerLocationDTO() {}

    public CustomerLocationDTO(
        String customerLocationUuid, 
        String customerLocationCode, 
        String customerUuid, 
        String locationTypeCode, 
        String locationRegionCode, 
        String lastModifiedUser
    ) {
        this.customerLocationUuid = customerLocationUuid;
        this.customerLocationCode = customerLocationCode;
        this.customerUuid = customerUuid;
        this.locationTypeCode = locationTypeCode;
        this.locationRegionCode = locationRegionCode;
        this.lastModifiedUser = lastModifiedUser;
    }

    public String getCustomerLocationUuid() {
        return customerLocationUuid;
    }

    public void setCustomerLocationUuid(String customerLocationUuid) {
        this.customerLocationUuid = customerLocationUuid;
    }

    public String getCustomerLocationCode() {
        return customerLocationCode;
    }

    public void setCustomerLocationCode(String customerLocationCode) {
        this.customerLocationCode = customerLocationCode;
    }

    public String getCustomerUuid() {
        return customerUuid;
    }

    public void setCustomerUuid(String customerUuid) {
        this.customerUuid = customerUuid;
    }

    public String getLocationTypeCode() {
        return locationTypeCode;
    }

    public void setLocationTypeCode(String locationTypeCode) {
        this.locationTypeCode = locationTypeCode;
    }

    public String getLocationRegionCode() {
        return locationRegionCode;
    }

    public void setLocationRegionCode(String locationRegionCode) {
        this.locationRegionCode = locationRegionCode;
    }

    public String getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(String lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.customerLocationUuid);
        hash = 97 * hash + Objects.hashCode(this.customerLocationCode);
        hash = 97 * hash + Objects.hashCode(this.customerUuid);
        hash = 97 * hash + Objects.hashCode(this.locationTypeCode);
        hash = 97 * hash + Objects.hashCode(this.locationRegionCode);
        hash = 97 * hash + Objects.hashCode(this.lastModifiedUser);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CustomerLocationDTO other = (CustomerLocationDTO) obj;
        if (!Objects.equals(this.customerLocationUuid, other.customerLocationUuid)) {
            return false;
        }
        if (!Objects.equals(this.customerLocationCode, other.customerLocationCode)) {
            return false;
        }
        if (!Objects.equals(this.customerUuid, other.customerUuid)) {
            return false;
        }
        if (!Objects.equals(this.locationTypeCode, other.locationTypeCode)) {
            return false;
        }
        if (!Objects.equals(this.locationRegionCode, other.locationRegionCode)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CustomerLocationDTO{" 
                
                    + "customerLocationUuid=" + customerLocationUuid 
                    + ", customerLocationCode=" + customerLocationCode 
                    + ", customerUuid=" + customerUuid 
                    + ", locationTypeCode=" + locationTypeCode 
                    + ", locationRegionCode=" + locationRegionCode 
                    + ", lastModifiedUser=" + lastModifiedUser 
                
                + '}';
    }

    
}
