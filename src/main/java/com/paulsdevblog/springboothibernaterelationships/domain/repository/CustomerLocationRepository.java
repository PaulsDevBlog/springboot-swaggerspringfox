
package com.paulsdevblog.springboothibernaterelationships.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.paulsdevblog.springboothibernaterelationships.domain.model.CustomerLocation;


/**
 * Spring Hibernate JPA repository for 
 * CustomerLocation
 * 
 * Uses named queries from model and spring-JPA method-name query definition language
 * 
 * Refer to https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
 * Refer to https://projects.spring.io/spring-data-jpa/ 
 * 
 * @author PaulsDevBlog.com
 */
public interface CustomerLocationRepository extends JpaRepository<CustomerLocation, Long> {
    
    public List<CustomerLocation> findAll();
    
    public List<CustomerLocation> findByCustomerLocationUuidAllIgnoreCaseOrderByCustomerLocationUuidAsc( @Param("customerLocationUuid") String customerLocationUuid );
    public List<CustomerLocation> findByCustomerLocationCodeAllIgnoreCaseOrderByCustomerLocationCodeAsc( @Param("customerLocationCode") String customerLocationCode );
    
}
