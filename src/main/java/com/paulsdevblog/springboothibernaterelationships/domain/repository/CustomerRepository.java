
package com.paulsdevblog.springboothibernaterelationships.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.paulsdevblog.springboothibernaterelationships.domain.model.Customer;


/**
 * Spring Hibernate JPA repository for 
 * Customer
 * 
 * Uses named queries from model and spring-JPA method-name query definition language
 * 
 * Refer to https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
 * Refer to https://projects.spring.io/spring-data-jpa/ 
 * 
 * @author PaulsDevBlog.com
 */
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    
    public List<Customer> findAll();
    
    public List<Customer> findByCustomerUuidAllIgnoreCaseOrderByCustomerUuidAsc( @Param("customerUuid") String customerUuid );
    public List<Customer> findByCustomerNameAllIgnoreCaseOrderByCustomerNameAsc( @Param("customerName") String customerName );
    
}
