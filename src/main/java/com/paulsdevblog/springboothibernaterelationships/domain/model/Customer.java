
package com.paulsdevblog.springboothibernaterelationships.domain.model;

import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;

import javax.persistence.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.GenericGenerator;

import org.springframework.core.annotation.*;
import org.springframework.beans.factory.annotation.*;

import com.paulsdevblog.springboothibernaterelationships.domain.views.EntityViews;
import com.paulsdevblog.utility.DateTimeConversion;

import com.paulsdevblog.springboothibernaterelationships.domain.model.CustomerLocation;

/**
 * Simple Spring-JPA-Hibernate entity example that represents table 
 * customer
 * 
 * @author PaulsDevBlog.com
 */
@Entity
@Table(name = "customer", uniqueConstraints = {
    @UniqueConstraint( columnNames = {"customer_name", "customer_ref"} )
})
@XmlRootElement
@JsonPropertyOrder(
    { 
        "customerRef",
        "customerUuid", 

        "customerName",
        
        "lastModifiedAction", 
        "lastModifiedUser", 
           
        "LastModifiedDt", 
        "LastModifiedDtAsIso8601", 
        "LastModifiedDtAsEpochSecond"
    }
)
public class Customer {
    
    @JsonIgnore
    private static final long serialVersionUID = 1L;
    
    //Yes there is both a customer_ref and customer_uuid, as 
    //MySQL aggregate functions (MIN,MAX etc.) only work with numeric key
    //This key is not sued for any other purposes than MySQL aggregate
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "customer_ref", nullable = false)
    private Long customerRef;
    
    //This is the priamry key we use for all joins and UUID for REST endpoints
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator" )
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "customer_uuid", nullable = false, length = 50)
    private String customerUuid;
    
    //This is the pointer back to the ManyToOne relationship defined in entity CustomerLocation
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer", fetch = FetchType.LAZY )
    private List<CustomerLocation> customerLocationList;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "customer_name", nullable = false, length = 50)
    private String customerName;
    
    @Size(max = 255)
    @Column(name = "last_modified_user", length = 255)
    private String lastModifiedUser;
    
    @Column(name = "last_modified_dt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDt;
    
    @Size(max = 25)
    @Column(name = "last_modified_action", length = 25)
    private String lastModifiedAction;

    public Customer() {}

    public Customer(
        Long customerRef, 
        String customerUuid, 
        String customerName, 
        String lastModifiedUser, 
        Date lastModifiedDt, 
        String lastModifiedAction
    ) {
        this.customerRef = customerRef;
        this.customerUuid = customerUuid;
        this.customerName = customerName;
        this.lastModifiedUser = lastModifiedUser;
        this.lastModifiedDt = lastModifiedDt;
        this.lastModifiedAction = lastModifiedAction;
    }

    @JsonView( { EntityViews.Details.class } )
    public Long getCustomerRef() {
        return customerRef;
    }

    public void setCustomerRef(Long customerRef) {
        this.customerRef = customerRef;
    }

    @JsonView( { EntityViews.List.class } )
    public String getCustomerUuid() {
        return customerUuid;
    }

    public void setCustomerUuid(String customerUuid) {
        this.customerUuid = customerUuid;
    }

    @JsonView( { EntityViews.EntityChildren.class } )
    public List<CustomerLocation> getCustomerLocationList() {
        return customerLocationList;
    }

    public void setCustomerLocationList(List<CustomerLocation> customerLocationList) {
        this.customerLocationList = customerLocationList;
    }

    @JsonView( { EntityViews.List.class } )
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    
    @JsonView( { EntityViews.Details.class } )
    public String getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(String lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    @JsonView( { EntityViews.Details.class } )
    public Date getLastModifiedDt() {
        return lastModifiedDt;
    }
    
    @JsonView( { EntityViews.Details.class } )
    public String getLastModifiedDtAsIso8601() {
        return DateTimeConversion.dateToISO8601( lastModifiedDt );
    }
    
    @JsonView( { EntityViews.Details.class } )
    public long getLastModifiedDtAsEpochSecond() {
        return DateTimeConversion.dateToEpochSecond( lastModifiedDt );
    }

    public void setLastModifiedDt(Date lastModifiedDt) {
        this.lastModifiedDt = lastModifiedDt;
    }

    @JsonView( { EntityViews.Details.class } )
    public String getLastModifiedAction() {
        return lastModifiedAction;
    }

    public void setLastModifiedAction(String lastModifiedAction) {
        this.lastModifiedAction = lastModifiedAction;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.customerRef);
        hash = 89 * hash + Objects.hashCode(this.customerUuid);
        hash = 89 * hash + Objects.hashCode(this.customerName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        if (!Objects.equals(this.customerUuid, other.customerUuid)) {
            return false;
        }
        if (!Objects.equals(this.customerName, other.customerName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Customer{" 
                
                    + "customerRef=" + String.valueOf( customerRef )
                    + ", customerUuid=" + String.valueOf( customerUuid )
                
                    + ", customerName=" + String.valueOf( customerName )
                
                    + ", lastModifiedUser=" + String.valueOf( lastModifiedUser )
                    + ", lastModifiedDtAsIso8601=" + this.getLastModifiedDtAsIso8601() 
                    + ", lastModifiedDtAsEpochSecond=" + String.valueOf( this.getLastModifiedDtAsEpochSecond() )
                    + ", lastModifiedAction=" + String.valueOf( this.lastModifiedAction ) 
                
                + '}';
    }

    
    
    
    
}
