
package com.paulsdevblog.springboothibernaterelationships.domain.model;

import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;

import javax.persistence.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.GenericGenerator;

import org.springframework.core.annotation.*;
import org.springframework.beans.factory.annotation.*;

import com.paulsdevblog.springboothibernaterelationships.domain.views.EntityViews;
import com.paulsdevblog.utility.DateTimeConversion;

/**
 * Simple Spring-JPA-Hibernate entity example that represents table 
 * customer_location
 * 
 * @author PaulsDevBlog.com
 */
@Entity
@Table(name = "customer_location", uniqueConstraints = {
    @UniqueConstraint( columnNames = {"customer_location_code","customer_location_ref" } )
})
@XmlRootElement
@JsonPropertyOrder(
    { 
        "customerLocationRef",
        "customerLocationUuid", 

        "customerLocationCode",
        
        "customerUuid",
        "locationTypeCode",
        "locationRegionCode",
        
        "lastModifiedAction", 
        "lastModifiedUser", 
           
        "LastModifiedDt", 
        "LastModifiedDtAsIso8601", 
        "LastModifiedDtAsEpochSecond"
    }
)
public class CustomerLocation {
    
    @JsonIgnore
    private static final long serialVersionUID = 1L;
    
    //Yes there is both a customer_location_ref and customer_location_uuid, as 
    //MySQL aggregate functions (MIN,MAX etc.) only work with numeric key column
    //This key is not used for any other purposes other than MySQL aggregation
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "customer_location_ref", nullable = false)
    private Long customerLocationRef;
    
    //This is the priamry key we use for all joins and UUID for REST endpoints
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator" )
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "customer_location_uuid", nullable = false, length = 50)
    private String customerLocationUuid;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "customer_location_code", nullable = false, length = 50)
    private String customerLocationCode;
    
    //customer relationship
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 5, max = 50)
    @Column(name = "customer_uuid", nullable = false, length = 50)
    private String customerUuid;
    
    @JoinColumn(name = "customer_uuid", referencedColumnName = "customer_uuid", updatable = false, insertable = false)
    @ManyToOne( fetch = FetchType.LAZY )
    private Customer customer;

    //location_type relationship
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "location_type_code", nullable = false, length = 25)
    private String locationTypeCode;
    
    @JoinColumn(name = "location_type_code", referencedColumnName = "location_type_code", updatable = false, insertable = false)
    @ManyToOne( fetch = FetchType.LAZY )
    private LocationType locationType;
    
    //location_region relationship
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "location_region_code", nullable = false, length = 25)
    private String locationRegionCode;
    
    @JoinColumn(name = "location_region_code", referencedColumnName = "location_region_code", updatable = false, insertable = false)
    @ManyToOne( fetch = FetchType.LAZY )
    private LocationRegion locationRegion;
    
    //Last modified by
    
    @Size(max = 255)
    @Column(name = "last_modified_user", length = 255)
    private String lastModifiedUser;
    
    @Column(name = "last_modified_dt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDt;
    
    @Size(max = 25)
    @Column(name = "last_modified_action", length = 25)
    private String lastModifiedAction;

    public CustomerLocation() {}

    public CustomerLocation(
        Long customerLocationRef, 
        String customerLocationUuid, 
        String customerLocationCode, 
        String customerUuid,
        String locationTypeCode, 
        String locationRegionCode, 
        String lastModifiedUser, 
        Date lastModifiedDt, 
        String lastModifiedAction
    ) {
        this.customerLocationRef = customerLocationRef;
        this.customerLocationUuid = customerLocationUuid;
        this.customerLocationCode = customerLocationCode;
        this.customerUuid = customerUuid;
        this.locationTypeCode = locationTypeCode;
        this.locationRegionCode = locationRegionCode;
        this.lastModifiedUser = lastModifiedUser;
        this.lastModifiedDt = lastModifiedDt;
        this.lastModifiedAction = lastModifiedAction;
    }

    @JsonView( { EntityViews.Details.class } )
    public Long getCustomerLocationRef() {
        return customerLocationRef;
    }

    public void setCustomerLocationRef(Long customerLocationRef) {
        this.customerLocationRef = customerLocationRef;
    }

    @JsonView( { EntityViews.List.class } )
    public String getCustomerLocationUuid() {
        return customerLocationUuid;
    }

    public void setCustomerLocationUuid(String customerLocationUuid) {
        this.customerLocationUuid = customerLocationUuid;
    }

    @JsonView( { EntityViews.List.class } )
    public String getCustomerLocationCode() {
        return customerLocationCode;
    }

    public void setCustomerLocationCode(String customerLocationCode) {
        this.customerLocationCode = customerLocationCode;
    }

    //customer relationships
    
    @JsonView( { EntityViews.List.class } )
    public String getCustomerUuid() {
        return customerUuid;
    }

    public void setCustomerUuid(String customerUuid) {
        this.customerUuid = customerUuid;
    }

    @JsonView( { EntityViews.EntityParents.class } )
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    //location_type relationships

    @JsonView( { EntityViews.List.class } )
    public String getLocationTypeCode() {
        return locationTypeCode;
    }

    public void setLocationTypeCode(String locationTypeCode) {
        this.locationTypeCode = locationTypeCode;
    }

    @JsonView( { EntityViews.EntityParents.class } )
    public LocationType getLocationType() {
        return locationType;
    }

    public void setLocationType(LocationType locationType) {
        this.locationType = locationType;
    }
    
    //location_region relationships
    
    @JsonView( { EntityViews.List.class } )
    public String getLocationRegionCode() {
        return locationRegionCode;
    }

    public void setLocationRegionCode(String locationRegionCode) {
        this.locationRegionCode = locationRegionCode;
    }

    @JsonView( { EntityViews.EntityParents.class } )
    public LocationRegion getLocationRegion() {
        return locationRegion;
    }

    public void setLocationRegion(LocationRegion locationRegion) {
        this.locationRegion = locationRegion;
    }

    @JsonView( { EntityViews.Details.class } )
    public String getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(String lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    @JsonView( { EntityViews.Details.class } )
    public Date getLastModifiedDt() {
        return lastModifiedDt;
    }
    
    @JsonView( { EntityViews.Details.class } )
    public String getLastModifiedDtAsIso8601() {
        return DateTimeConversion.dateToISO8601( lastModifiedDt );
    }
    
    @JsonView( { EntityViews.Details.class } )
    public long getLastModifiedDtAsEpochSecond() {
        return DateTimeConversion.dateToEpochSecond( lastModifiedDt );
    }

    public void setLastModifiedDt(Date lastModifiedDt) {
        this.lastModifiedDt = lastModifiedDt;
    }

    @JsonView( { EntityViews.Details.class } )
    public String getLastModifiedAction() {
        return lastModifiedAction;
    }

    public void setLastModifiedAction(String lastModifiedAction) {
        this.lastModifiedAction = lastModifiedAction;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.customerLocationRef);
        hash = 97 * hash + Objects.hashCode(this.customerLocationUuid);
        hash = 97 * hash + Objects.hashCode(this.customerLocationCode);
        hash = 97 * hash + Objects.hashCode(this.customerUuid);
        hash = 97 * hash + Objects.hashCode(this.locationTypeCode);
        hash = 97 * hash + Objects.hashCode(this.locationRegionCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CustomerLocation other = (CustomerLocation) obj;
        if (!Objects.equals(this.customerLocationUuid, other.customerLocationUuid)) {
            return false;
        }
        if (!Objects.equals(this.customerLocationCode, other.customerLocationCode)) {
            return false;
        }
        if (!Objects.equals(this.customerUuid, other.customerUuid)) {
            return false;
        }
        if (!Objects.equals(this.locationTypeCode, other.locationTypeCode)) {
            return false;
        }
        if (!Objects.equals(this.locationRegionCode, other.locationRegionCode)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CustomerLocation{" 
                
                    + "customerLocationRef=" + customerLocationRef 
                    + ", customerLocationUuid=" + customerLocationUuid 
                
                    + ", customerLocationCode=" + customerLocationCode 
                
                    + ", customerUuid=" + customerUuid 
                    + ", locationTypeCode=" + locationTypeCode 
                    + ", locationRegionCode=" + locationRegionCode 
                
                
                    + ", lastModifiedUser=" + String.valueOf( lastModifiedUser )
                    + ", lastModifiedDtAsIso8601=" + this.getLastModifiedDtAsIso8601() 
                    + ", lastModifiedDtAsEpochSecond=" + String.valueOf( this.getLastModifiedDtAsEpochSecond() )
                    + ", lastModifiedAction=" + String.valueOf( this.lastModifiedAction ) 
                
                + '}';
    }

}
