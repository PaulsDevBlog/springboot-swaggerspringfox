
package com.paulsdevblog.springboothibernaterelationships.controller;

import com.fasterxml.jackson.annotation.JsonView;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paulsdevblog.springboothibernaterelationships.domain.model.LocationType;
import com.paulsdevblog.springboothibernaterelationships.domain.service.LocationTypeService;

import com.paulsdevblog.springboothibernaterelationships.domain.views.EntityViews;
import com.paulsdevblog.exceptions.ServiceLayerDataException;


/**
 * Simple REST controller example that uses LocationTypeService and
 * exposes respective CRUD operations
 *
 * @author PaulsDevBlog.com
 */
@RestController
@RequestMapping("/locationtype")
public class LocationTypeController {
    
    private Logger logger = LoggerFactory.getLogger(LocationTypeController.class );
    
    @Autowired
    private LocationTypeService locationTypeService;
    
    public LocationTypeController( LocationTypeService locationTypeService ){
        this.locationTypeService = locationTypeService;

    }
    
    /**
     * REST GET Find list of ALL LocationType
     * 
     * @return list of LocationType 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List all LocationType entities")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = LocationType.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.List.class)
    @RequestMapping(value={"/list" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<LocationType>> findAllLocationType() throws ServiceLayerDataException {

        //Get list from service
        List<LocationType> listEntities = this.locationTypeService.findAll();
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<LocationType>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST GET Find list of LocationType matching locationTypeCode
     * 
     * @param locationTypeCode parameter code
     * @return list of LocationType 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List all LocationType entities for LocationTypeCode")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = LocationType.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.Details.class)
    @RequestMapping(value={"/code/{locationtypecode}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<LocationType>> findByLocationTypeCode( 
                @ApiParam (value = "locationTypeCode", required = true) 
                @PathVariable( value = "locationtypecode", required = true ) String locationTypeCode 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        locationTypeCode = String.valueOf( locationTypeCode );
        
        //Get list from service
        List<LocationType> listEntities = this.locationTypeService.findByLocationTypeCode(locationTypeCode);
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<LocationType>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST GET Find list of LocationType matching locationTypeCode and
     * the CustomerLocation entities with a OneToMany relationship to this code
     * 
     * @param locationTypeCode parameter code
     * @return list of LocationType 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List all LocationType entity relationships for LocationTypeCode")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = LocationType.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.EntityChildren.class)
    @RequestMapping(value={"/relationships/{locationtypecode}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<LocationType>> findByLocationTypeCodeRelationships( 
                @ApiParam (value = "locationTypeCode", required = true) 
                @PathVariable( value = "locationtypecode", required = true ) String locationTypeCode 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        locationTypeCode = String.valueOf( locationTypeCode );
        
        //Get list from service
        List<LocationType> listEntities = this.locationTypeService.findByLocationTypeCode(locationTypeCode);
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<LocationType>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST GET Find list of LocationType matching locationTypeUuid
     * 
     * @param locationTypeUuid parameter UUID
     * @return list of LocationType 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List all LocationType entities for LocationTypeUuid")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = LocationType.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.Details.class)
    @RequestMapping(value={"/uuid/{locationtypeuuid}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<LocationType>> findByLocationTypeUuid( 
                @ApiParam (value = "locationTypeUuid", required = true) 
                @PathVariable( value = "locationtypeuuid", required = true ) String locationTypeUuid 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        locationTypeUuid = String.valueOf( locationTypeUuid );
        
        //Get list from service
        List<LocationType> listEntities = this.locationTypeService.findByLocationTypeUuid( locationTypeUuid );
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<LocationType>>( listEntities, HttpStatus.OK );
        }
    }
    
}
