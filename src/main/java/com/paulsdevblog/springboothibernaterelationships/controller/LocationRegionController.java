
package com.paulsdevblog.springboothibernaterelationships.controller;

import com.fasterxml.jackson.annotation.JsonView;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paulsdevblog.springboothibernaterelationships.domain.model.LocationRegion;
import com.paulsdevblog.springboothibernaterelationships.domain.service.LocationRegionService;

import com.paulsdevblog.springboothibernaterelationships.domain.views.EntityViews;
import com.paulsdevblog.exceptions.ServiceLayerDataException;


/**
 * Simple REST controller example that uses LocationRegionService and
 * exposes respective CRUD operations
 *
 * @author PaulsDevBlog.com
 */
@RestController
@RequestMapping("/locationregion")
public class LocationRegionController {
    
    private Logger logger = LoggerFactory.getLogger(LocationRegionController.class );
    
    @Autowired
    private LocationRegionService locationRegionService;
    
    public LocationRegionController( LocationRegionService locationRegionService ){
        this.locationRegionService = locationRegionService;

    }
    
    /**
     * REST GET Find list of ALL LocationRegion
     * 
     * @return list of LocationRegion 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List all LocationRegion entities")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = LocationRegion.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.List.class)
    @RequestMapping(value={"/list" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<LocationRegion>> findAllLocationRegion() throws ServiceLayerDataException {

        //Get list from service
        List<LocationRegion> listEntities = this.locationRegionService.findAll();
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<LocationRegion>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST GET Find list of LocationRegion matching locationRegionCode
     * 
     * @param locationRegionCode parameter code
     * @return list of LocationRegion 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List all LocationRegion entities for locationRegionCode")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = LocationRegion.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.Details.class)
    @RequestMapping(value={"/code/{locationregioncode}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<LocationRegion>> findByLocationRegionCode( 
            @ApiParam (value = "locationRegionCode", required = true) 
            @PathVariable( value = "locationregioncode", required = true ) String locationRegionCode 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        locationRegionCode = String.valueOf( locationRegionCode );
        
        //Get list from service
        List<LocationRegion> listEntities = this.locationRegionService.findByLocationRegionCode(locationRegionCode);
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<LocationRegion>>( listEntities, HttpStatus.OK );
        }
    }
    
     /**
     * REST GET Find list of LocationRegion matching locationRegionCode and
     * the CustomerLocation entities with a OneToMany relationship to this code
     * 
     * @param locationRegionCode parameter code
     * @return list of LocationRegion 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List all LocationRegion entity relationships for locationRegionCode")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = LocationRegion.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.EntityChildren.class)
    @RequestMapping(value={"/relationships/{locationregioncode}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<LocationRegion>> findByLocationRegionCodeRelationships( 
                @ApiParam (value = "locationRegionCode", required = true) 
                @PathVariable( value = "locationregioncode", required = true ) String locationRegionCode 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        locationRegionCode = String.valueOf( locationRegionCode );
        
        //Get list from service
        List<LocationRegion> listEntities = this.locationRegionService.findByLocationRegionCode(locationRegionCode);
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<LocationRegion>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST GET Find list of LocationRegion matching locationRegionUuid
     * 
     * @param locationRegionUuid parameter UUID
     * @return list of LocationRegion 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List all LocationRegion entities for locationRegionUuid")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = LocationRegion.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.Details.class)
    @RequestMapping(value={"/uuid/{locationregionuuid}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<LocationRegion>> findByLocationRegionUuid( 
                @ApiParam (value = "locationRegionUuid", required = true) 
                @PathVariable( value = "locationregionuuid", required = true ) String locationRegionUuid 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        locationRegionUuid = String.valueOf( locationRegionUuid );
        
        //Get list from service
        List<LocationRegion> listEntities = this.locationRegionService.findByLocationRegionUuid( locationRegionUuid );
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<LocationRegion>>( listEntities, HttpStatus.OK );
        }
    }
    
}
