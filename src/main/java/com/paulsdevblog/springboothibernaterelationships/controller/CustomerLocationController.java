
package com.paulsdevblog.springboothibernaterelationships.controller;

import com.fasterxml.jackson.annotation.JsonView;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paulsdevblog.springboothibernaterelationships.domain.model.CustomerLocation;
import com.paulsdevblog.springboothibernaterelationships.domain.service.CustomerLocationService;
import com.paulsdevblog.springboothibernaterelationships.domain.dto.CustomerLocationDTO;

import com.paulsdevblog.springboothibernaterelationships.domain.views.EntityViews;
import com.paulsdevblog.exceptions.ServiceLayerDataException;


/**
 * Simple REST controller example that uses CustomerLocationService and
 * exposes respective CRUD operations
 *
 * @author PaulsDevBlog.com
 */
@RestController
@RequestMapping("/customerlocation")
public class CustomerLocationController {
    
    private Logger logger = LoggerFactory.getLogger(CustomerLocationController.class );
    
    @Autowired
    private CustomerLocationService customerLocationService;
    
    public CustomerLocationController( CustomerLocationService customerLocationService ){
        this.customerLocationService = customerLocationService;

    }
    
    /**
     * REST GET Find list of ALL CustomerLocation
     * 
     * @return list of CustomerLocation 
     * @throws ServiceLayerDataException 
     */
     
    @ApiOperation(value = "List all CustomerLocation entities ")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = CustomerLocation.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.List.class)
    @RequestMapping(value={"/list" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<CustomerLocation>> findAllCustomerLocation() throws ServiceLayerDataException {

        //Get list from service
        List<CustomerLocation> listEntities = this.customerLocationService.findAll();
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<CustomerLocation>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST GET Find list of CustomerLocation matching customerLocationCode
     * 
     * @param customerLocationCode parameter code
     * @return list of CustomerLocation 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List all CustomerLocation entities for customerLocationCode")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = CustomerLocation.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.Details.class)
    @RequestMapping(value={"/code/{customerlocationcode}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<CustomerLocation>> findByCustomerLocationCode( 
                @ApiParam (value = "customerLocationCode", required = true) 
                @PathVariable( value = "customerlocationcode", required = true ) String customerLocationCode 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        customerLocationCode = String.valueOf( customerLocationCode );
        
        //Get list from service
        List<CustomerLocation> listEntities = this.customerLocationService.findByCustomerLocationCode(customerLocationCode);
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<CustomerLocation>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST GET Find list of CustomerLocation matching customerLocationUuid
     * 
     * @param customerLocationUuid parameter UUID
     * @return list of CustomerLocation 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List all CustomerLocation entities for customerLocationUuid")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = CustomerLocation.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.Details.class)
    @RequestMapping(value={"/uuid/{customerlocationuuid}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<CustomerLocation>> findByCustomerLocationUuid( 
                @ApiParam (value = "customerLocationUuid", required = true) 
                @PathVariable( value = "customerlocationuuid", required = true ) String customerLocationUuid 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        customerLocationUuid = String.valueOf( customerLocationUuid );
        
        //Get list from service
        List<CustomerLocation> listEntities = this.customerLocationService.findByCustomerLocationUuid( customerLocationUuid );
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<CustomerLocation>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST GET Find list of CustomerLocation matching customerLocationUuid and 
     * all Parent relationships (ManyToOne) defined
     * 
     * @param customerLocationUuid parameter UUID
     * @return list of CustomerLocation 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List all CustomerLocation entity relationships for customerLocationUuid")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = CustomerLocation.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.EntityParents.class)
    @RequestMapping(value={"/relationships/{customerlocationuuid}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<CustomerLocation>> findByCustomerLocationUuidRelationships( 
                @ApiParam (value = "customerLocationUuid", required = true) 
                @PathVariable( value = "customerlocationuuid", required = true ) String customerLocationUuid 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        customerLocationUuid = String.valueOf( customerLocationUuid );
        
        //Get list from service
        List<CustomerLocation> listEntities = this.customerLocationService.findByCustomerLocationUuid( customerLocationUuid );
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<CustomerLocation>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST POST insert NEW CustomerLocation
     * 
     * @param CustomerLocationDTO parameter 
     * @return CustomerLocation inserted
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "INSERT NEW CustomerLocation entity")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = CustomerLocation.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.List.class)
    @RequestMapping(value={ "" }, 
                    method=RequestMethod.POST,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<CustomerLocation> insertCustomerLocation( 
                @ApiParam (value = "CustomerLocationDTO", required = true) 
                @RequestBody(required = true) CustomerLocationDTO customerLocationDTO 
    ) throws ServiceLayerDataException{

        //Get list from service
        CustomerLocation retCustomerLocation = this.customerLocationService.insertCustomerLocation( customerLocationDTO );
        
        if ( retCustomerLocation == null ) {
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<CustomerLocation>( retCustomerLocation, HttpStatus.OK );
        }
    }
    
    
    /**
     * REST PUT Updates EXISTING CustomerLocation
     * 
     * @param CustomerLocationDTO parameter 
     * @return CustomerLocation updated
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "UPDATE existing CustomerLocation entity")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = CustomerLocation.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.Details.class)
    @RequestMapping(value={ "/uuid/{customerlocationuuid}" },
                    method=RequestMethod.PUT,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<CustomerLocation> updateCustomer( 
                    @ApiParam (value = "customerLocationUuid", required = true) 
                    @PathVariable( value = "customerlocationuuid", required = true ) String customerLocationUuid,
            
                    @ApiParam (value = "CustomerLocationDTO", required = true) 
                    @RequestBody(required = true) CustomerLocationDTO customerLocationDTO 
    ) throws ServiceLayerDataException{

        //Get list from service
        CustomerLocation retCustomerLocation = this.customerLocationService.updateCustomerLocation( customerLocationDTO );
        
        if ( retCustomerLocation == null ) {
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<CustomerLocation>( retCustomerLocation, HttpStatus.OK );
        }
    }
    
}
