
package com.paulsdevblog.springboothibernaterelationships.controller;

import com.fasterxml.jackson.annotation.JsonView;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.swagger.annotations.*;

import com.paulsdevblog.springboothibernaterelationships.domain.model.Customer;
import com.paulsdevblog.springboothibernaterelationships.domain.service.CustomerService;

import com.paulsdevblog.springboothibernaterelationships.domain.views.EntityViews;
import com.paulsdevblog.exceptions.ServiceLayerDataException;
import com.paulsdevblog.springboothibernaterelationships.domain.dto.CustomerDTO;


/**
 * Simple REST controller example that uses CustomerService and
 * exposes respective CRUD operations
 *
 * @author PaulsDevBlog.com
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {
    
    private Logger logger = LoggerFactory.getLogger(CustomerController.class );
    
    @Autowired
    private CustomerService customerService;
    
    public CustomerController( CustomerService customerService ){
        this.customerService = customerService;

    }
    
    /**
     * REST GET Find list of ALL Customer
     * 
     * @return list of Customer 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "List Customer entities")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = Customer.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.List.class)
    @RequestMapping(value={"/list" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<Customer>> findAllCustomer() throws ServiceLayerDataException {

        //Get list from service
        List<Customer> listEntities = this.customerService.findAll();
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<Customer>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST GET Find list of Customer matching customerName
     * 
     * @param customerName parameter code
     * @return list of Customer 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "Find Customer entities for CustomerName  ")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = Customer.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.Details.class)
    @RequestMapping(value={"/name/{customername}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<Customer>> findByCustomerCode( 
                @ApiParam (value = "customerName", required = true) 
                @PathVariable( value = "customername", required = true ) String customerName 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        customerName = String.valueOf( customerName );
        
        //Get list from service
        List<Customer> listEntities = this.customerService.findByCustomerName(customerName);
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<Customer>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST GET Find list of Customer matching customerUuid
     * 
     * @param customerUuid parameter UUID
     * @return list of Customer 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "Find Customer entities for CustomerUUID  ")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = Customer.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.Details.class)
    @RequestMapping(value={ "/uuid/{customeruuid}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<Customer>> findByCustomerUuid( 
                @ApiParam (value = "customerUuid", required = true) 
                @PathVariable( value = "customeruuid", required = true ) String customerUuid 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        customerUuid = String.valueOf( customerUuid );
        
        //Get list from service
        List<Customer> listEntities = this.customerService.findByCustomerUuid( customerUuid );
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<Customer>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST GET Find list of Customer matching customerUuid and all child 
     * relationship mappings
     * 
     * @param customerUuid parameter UUID
     * @return list of Customer 
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "Find Customer entity Relationships for CustomerUUID  ")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = Customer.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.EntityChildren.class)
    @RequestMapping(value={"/relationships/{customeruuid}" }, 
                    method=RequestMethod.GET,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<List<Customer>> findByCustomerUuidAllChildren( 
            @ApiParam (value = "customerUuid", required = true) 
            @PathVariable( value = "customeruuid", required = true ) String customerUuid 
    ) throws ServiceLayerDataException {

        //Avoid nullpointer and ensure we have valid string value
        customerUuid = String.valueOf( customerUuid );
        
        //Get list from service
        List<Customer> listEntities = this.customerService.findByCustomerUuid( customerUuid );
        
        if ( listEntities == null ) {
            return ResponseEntity.status( HttpStatus.NOT_FOUND ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<List<Customer>>( listEntities, HttpStatus.OK );
        }
    }
    
    /**
     * REST POST insert NEW Customer
     * 
     * @param CustomerDTO parameter 
     * @return Customer inserted
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "Insert NEW Customer entity ")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = Customer.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.List.class)
    @RequestMapping(value={ "" }, 
                    method=RequestMethod.POST,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<Customer> insertCustomer( 
            @ApiParam ( value = "CustomerDTO", required = true) 
            @RequestBody(required = true) CustomerDTO customerDTO 
    ) throws ServiceLayerDataException{

        //Get list from service
        Customer retCustomer = this.customerService.insertCustomer( customerDTO );
        
        if ( retCustomer == null ) {
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<Customer>( retCustomer, HttpStatus.OK );
        }
    }
    
    /**
     * REST PUT Updates EXISTING Customer
     * 
     * @param CustomerDTO parameter 
     * @return Customer updated
     * @throws ServiceLayerDataException 
     */
    @ApiOperation(value = "UPDATE existing Customer entity for CustomerUUID ")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = Customer.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @JsonView(EntityViews.Details.class)
    @RequestMapping(value={ "/uuid/{customeruuid}" },
                    method=RequestMethod.PUT,
                    produces = { MediaType.APPLICATION_JSON_VALUE }
                    )
    public ResponseEntity<Customer> updateCustomer( 
                    @ApiParam ( value = "customerUuid", required = true) 
                    @PathVariable( value = "customeruuid", required = true ) String customerUuid,
            
                    @ApiParam ( value = "CustomerDTO", required = true) 
                    @RequestBody(required = true) CustomerDTO customerDTO 
    ) throws ServiceLayerDataException{

        //Get list from service
        Customer retCustomer = this.customerService.updateCustomer( customerDTO );
        
        if ( retCustomer == null ) {
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).build(); //return 404, with null body
        } else {
            return new ResponseEntity<Customer>( retCustomer, HttpStatus.OK );
        }
    }
    
    
}
