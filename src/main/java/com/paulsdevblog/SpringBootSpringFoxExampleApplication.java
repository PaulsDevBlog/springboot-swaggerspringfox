package com.paulsdevblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main entry point for application
 * 
 * @author PaulsDevBlog.com
 */
@SpringBootApplication
@ComponentScan({    
                "com.paulsdevblog",
                "com.paulsdevblog.config",
                "com.paulsdevblog.springboothibernaterelationships.domain",
                "com.paulsdevblog.springboothibernaterelationships.domain.model",
                "com.paulsdevblog.springboothibernaterelationships.domain.repository",
                "com.paulsdevblog.springboothibernaterelationships.domain.service",
                "com.paulsdevblog.springboothibernaterelationships.domain.views",
                "com.paulsdevblog.utility"
})
public class SpringBootSpringFoxExampleApplication {
    
    public static final Logger logger = LoggerFactory.getLogger(SpringBootSpringFoxExampleApplication.class );

    public static void main(String[] args) {
        
        SpringApplication.run(SpringBootSpringFoxExampleApplication.class, args);
        
        logger.info("--Application Started--");
        logger.info("SpringBoot and Hibernate Entity Relationships Micro-Service!");
    }
}
