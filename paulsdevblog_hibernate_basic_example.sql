CREATE DATABASE  IF NOT EXISTS `paulsdevblog_hibernate_basic_example` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `paulsdevblog_hibernate_basic_example`;
-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: paulsdevblog_hibernate_basic_example
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `customer_ref` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_uuid` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `customer_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_modified_user` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified_dt` datetime DEFAULT NULL,
  `last_modified_action` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'INSERT',
  PRIMARY KEY (`customer_ref`),
  UNIQUE KEY `idx_customer_uuid_UNIQUE` (`customer_uuid`),
  UNIQUE KEY `idx_customer_name` (`customer_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1003 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`customer_ref`, `customer_uuid`, `customer_name`, `last_modified_user`, `last_modified_dt`, `last_modified_action`) VALUES (1000,'987e86ff-c885-11e7-b411-000c29f66550','ACME Supply',NULL,'2017-11-13 15:16:17','INSERT');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `paulsdevblog_hibernate_basic_example`.`customer_BEFORE_INSERT` BEFORE INSERT ON `customer` FOR EACH ROW
BEGIN
	-- ##################################################################################
	-- DECLAREs must be command first lines 
    -- ##################################################################################

	DECLARE var_trigger_current_dt DATETIME;
    DECLARE var_trigger_new_uuid VARCHAR(50); 
    
	-- ##################################################################################
    -- SET variables to unsafe functions as a work-around to make trigger replication safe
    -- ##################################################################################
    
    SET var_trigger_current_dt = ( UTC_TIMESTAMP() );
    SET var_trigger_new_uuid = ( SELECT UUID() );
    
	-- ##################################################################################
    -- SET last_modified values
    -- ##################################################################################
    
	SET NEW.last_modified_action = 'INSERT';
    SET NEW.last_modified_dt = var_trigger_current_dt ;
    
	-- ##################################################################################
    -- SET uuid key
    -- ##################################################################################
    SET NEW.customer_uuid = IFNULL( NEW.customer_uuid , var_trigger_new_uuid );
    
    IF ( NEW.customer_uuid = '' ) THEN 
		SET NEW.customer_uuid = var_trigger_new_uuid;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `paulsdevblog_hibernate_basic_example`.`customer_BEFORE_UPDATE` BEFORE UPDATE ON `customer` FOR EACH ROW
BEGIN
	-- ##################################################################################
	-- DECLAREs must be command first lines 
    -- ##################################################################################

	DECLARE var_trigger_current_dt DATETIME;
    DECLARE var_trigger_new_uuid VARCHAR(50); 
    
	-- ##################################################################################
    -- SET variables to unsafe functions as a work-around to make trigger replication safe
    -- ##################################################################################
    
    SET var_trigger_current_dt = ( UTC_TIMESTAMP() );
    SET var_trigger_new_uuid = ( SELECT UUID() );
    
	-- ##################################################################################
    -- SET last_modified values
    -- ##################################################################################
    
	SET NEW.last_modified_action = 'UPDATE';
    SET NEW.last_modified_dt = var_trigger_current_dt ;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `customer_location`
--

DROP TABLE IF EXISTS `customer_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_location` (
  `customer_location_ref` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_location_uuid` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `customer_location_code` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `customer_uuid` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `location_type_code` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'UNDEFINED',
  `location_region_code` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'UNDEFINED',
  `last_modified_user` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified_dt` datetime DEFAULT NULL,
  `last_modified_action` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'INSERT',
  PRIMARY KEY (`customer_location_ref`),
  UNIQUE KEY `idx_customer_location_uuid_UNIQUE` (`customer_location_uuid`),
  UNIQUE KEY `idx_customer_location_code` (`customer_location_code`),
  KEY `idx_customer_type_code` (`location_type_code`),
  KEY `idx_customer_region_code` (`location_region_code`),
  KEY `idx_customer_uuid` (`customer_uuid`),
  CONSTRAINT `fk_customer_location_to_customer` FOREIGN KEY (`customer_uuid`) REFERENCES `customer` (`customer_uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_location_to_location_region` FOREIGN KEY (`location_region_code`) REFERENCES `location_region` (`location_region_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_location_to_location_type` FOREIGN KEY (`location_type_code`) REFERENCES `location_type` (`location_type_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1011 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_location`
--

LOCK TABLES `customer_location` WRITE;
/*!40000 ALTER TABLE `customer_location` DISABLE KEYS */;
INSERT INTO `customer_location` (`customer_location_ref`, `customer_location_uuid`, `customer_location_code`, `customer_uuid`, `location_type_code`, `location_region_code`, `last_modified_user`, `last_modified_dt`, `last_modified_action`) VALUES (1000,'166616d1-c887-11e7-b411-000c29f66550','ACME_HQ','987e86ff-c885-11e7-b411-000c29f66550','HEADQUARTERS','NORTH_AMERICA','PaulsDevBlog.com','2017-11-13 15:26:58','INSERT'),(1001,'16665b23-c887-11e7-b411-000c29f66550','ACME_SOUTH_DIST01','987e86ff-c885-11e7-b411-000c29f66550','DISTRIBUTOR','SOUTH_EAST','PaulsDevBlog.com','2017-11-13 15:26:58','INSERT'),(1002,'16669f8c-c887-11e7-b411-000c29f66550','ACME_SOUTH_DIST02','987e86ff-c885-11e7-b411-000c29f66550','DISTRIBUTOR','SOUTH_WEST','PaulsDevBlog.com','2017-11-13 15:26:58','INSERT'),(1003,'16675742-c887-11e7-b411-000c29f66550','ACME_STORE_0070','987e86ff-c885-11e7-b411-000c29f66550','STORE_FRONT','SOUTH_WEST','PaulsDevBlog.com','2017-11-13 15:26:58','INSERT'),(1004,'1667a7de-c887-11e7-b411-000c29f66550','ACME_STORE_0080','987e86ff-c885-11e7-b411-000c29f66550','STORE_FRONT','SOUTH_WEST','PaulsDevBlog.com','2017-11-13 15:26:58','INSERT'),(1005,'1667e2e1-c887-11e7-b411-000c29f66550','ACME_STORE_0090','987e86ff-c885-11e7-b411-000c29f66550','STORE_FRONT','SOUTH_WEST','PaulsDevBlog.com','2017-11-13 15:26:58','INSERT'),(1006,'16681706-c887-11e7-b411-000c29f66550','ACME_STORE_0100','987e86ff-c885-11e7-b411-000c29f66550','STORE_FRONT','SOUTH_EAST','PaulsDevBlog.com','2017-11-13 15:26:58','INSERT'),(1007,'1668499c-c887-11e7-b411-000c29f66550','ACME_STORE_0110','987e86ff-c885-11e7-b411-000c29f66550','STORE_FRONT','SOUTH_EAST','PaulsDevBlog.com','2017-11-13 15:26:58','INSERT'),(1008,'16687928-c887-11e7-b411-000c29f66550','XYZ_NA_0001','987e86ff-c885-11e7-b411-000c29f66550','RESELLER','NORTH_AMERICA','PaulsDevBlog.com','2017-11-13 15:26:58','INSERT'),(1009,'2f181ecb-c887-11e7-b411-000c29f66550','BBB_SW_0001','987e86ff-c885-11e7-b411-000c29f66550','RESELLER','SOUTH_WEST','PaulsDevBlog.com','2017-11-13 15:27:39','INSERT');
/*!40000 ALTER TABLE `customer_location` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `paulsdevblog_hibernate_basic_example`.`customer_location_BEFORE_INSERT` BEFORE INSERT ON `customer_location` FOR EACH ROW
BEGIN
	-- ##################################################################################
	-- DECLAREs must be command first lines 
    -- ##################################################################################

	DECLARE var_trigger_current_dt DATETIME;
    DECLARE var_trigger_new_uuid VARCHAR(50); 
    
	-- ##################################################################################
    -- SET variables to unsafe functions as a work-around to make trigger replication safe
    -- ##################################################################################
    
    SET var_trigger_current_dt = ( UTC_TIMESTAMP() );
    SET var_trigger_new_uuid = ( SELECT UUID() );
    
	-- ##################################################################################
    -- SET last_modified values
    -- ##################################################################################
    
	SET NEW.last_modified_action = 'INSERT';
    SET NEW.last_modified_dt = var_trigger_current_dt ;
    
	-- ##################################################################################
    -- SET uuid key
    -- ##################################################################################
    SET NEW.customer_location_uuid = IFNULL( NEW.customer_location_uuid , var_trigger_new_uuid );
    
    IF ( NEW.customer_location_uuid = '' ) THEN 
		SET NEW.customer_location_uuid = var_trigger_new_uuid;
	END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `paulsdevblog_hibernate_basic_example`.`customer_location_BEFORE_UPDATE` BEFORE UPDATE ON `customer_location` FOR EACH ROW
BEGIN
	-- ##################################################################################
	-- DECLAREs must be command first lines 
    -- ##################################################################################

	DECLARE var_trigger_current_dt DATETIME;
    DECLARE var_trigger_new_uuid VARCHAR(50); 
    
	-- ##################################################################################
    -- SET variables to unsafe functions as a work-around to make trigger replication safe
    -- ##################################################################################
    
    SET var_trigger_current_dt = ( UTC_TIMESTAMP() );
    SET var_trigger_new_uuid = ( SELECT UUID() );
    
	-- ##################################################################################
    -- SET last_modified values
    -- ##################################################################################
    
	SET NEW.last_modified_action = 'UPDATE';
    SET NEW.last_modified_dt = var_trigger_current_dt ;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `location_region`
--

DROP TABLE IF EXISTS `location_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_region` (
  `location_region_ref` bigint(20) NOT NULL AUTO_INCREMENT,
  `location_region_uuid` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `location_region_code` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `location_region_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_modified_user` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified_dt` datetime DEFAULT NULL,
  `last_modified_action` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'INSERT',
  PRIMARY KEY (`location_region_ref`),
  UNIQUE KEY `idx_location_region_uuid_UNIQUE` (`location_region_uuid`),
  UNIQUE KEY `idx_location_region_name` (`location_region_name`),
  UNIQUE KEY `location_region_code_UNIQUE` (`location_region_code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_region`
--

LOCK TABLES `location_region` WRITE;
/*!40000 ALTER TABLE `location_region` DISABLE KEYS */;
INSERT INTO `location_region` (`location_region_ref`, `location_region_uuid`, `location_region_code`, `location_region_name`, `last_modified_user`, `last_modified_dt`, `last_modified_action`) VALUES (1,'bce59311-c883-11e7-b411-000c29f66550','NORTHERN_US','Nothern_US','DEFAULT','2017-11-13 15:02:59','INSERT'),(2,'bce5151b-c883-11e7-b411-000c29f66550','SOUTH_EAST','South East','DEFAULT','2017-11-13 15:20:29','UPDATE'),(3,'bce55473-c883-11e7-b411-000c29f66550','SOUTH_WEST','South West','DEFAULT','2017-11-13 15:20:29','UPDATE'),(4,'bce4cfbe-c883-11e7-b411-000c29f66550','UNDEFINED','Undefined','DEFAULT','2017-11-13 15:02:59','INSERT'),(5,'2f0843ba-c886-11e7-b411-000c29f66550','NORTH_AMERICA','North America','DEFAULT','2017-11-13 15:20:29','INSERT');
/*!40000 ALTER TABLE `location_region` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `paulsdevblog_hibernate_basic_example`.`location_region_BEFORE_INSERT` BEFORE INSERT ON `location_region` FOR EACH ROW
BEGIN
	-- ##################################################################################
	-- DECLAREs must be command first lines 
    -- ##################################################################################

	DECLARE var_trigger_current_dt DATETIME;
    DECLARE var_trigger_new_uuid VARCHAR(50); 
    
	-- ##################################################################################
    -- SET variables to unsafe functions as a work-around to make trigger replication safe
    -- ##################################################################################
    
    SET var_trigger_current_dt = ( UTC_TIMESTAMP() );
    SET var_trigger_new_uuid = ( SELECT UUID() );
    
	-- ##################################################################################
    -- SET last_modified values
    -- ##################################################################################
    
	SET NEW.last_modified_action = 'INSERT';
    SET NEW.last_modified_dt = var_trigger_current_dt ;
    
	-- ##################################################################################
    -- SET uuid key
    -- ##################################################################################
    SET NEW.location_region_uuid = IFNULL( NEW.location_region_uuid , var_trigger_new_uuid );
    
    IF ( NEW.location_region_uuid = '' ) THEN 
		SET NEW.location_region_uuid = var_trigger_new_uuid;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `paulsdevblog_hibernate_basic_example`.`location_region_BEFORE_UPDATE` BEFORE UPDATE ON `location_region` FOR EACH ROW
BEGIN
	-- ##################################################################################
	-- DECLAREs must be command first lines 
    -- ##################################################################################

	DECLARE var_trigger_current_dt DATETIME;
    DECLARE var_trigger_new_uuid VARCHAR(50); 
    
	-- ##################################################################################
    -- SET variables to unsafe functions as a work-around to make trigger replication safe
    -- ##################################################################################
    
    SET var_trigger_current_dt = ( UTC_TIMESTAMP() );
    SET var_trigger_new_uuid = ( SELECT UUID() );
    
	-- ##################################################################################
    -- SET last_modified values
    -- ##################################################################################
    
	SET NEW.last_modified_action = 'UPDATE';
    SET NEW.last_modified_dt = var_trigger_current_dt ;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `location_type`
--

DROP TABLE IF EXISTS `location_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_type` (
  `location_type_uuid` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `location_type_code` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `location_type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_modified_user` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified_dt` datetime DEFAULT NULL,
  `last_modified_action` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'INSERT',
  PRIMARY KEY (`location_type_code`),
  UNIQUE KEY `idx_location_type_uuid_UNIQUE` (`location_type_uuid`),
  UNIQUE KEY `idx_location_type_name` (`location_type_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_type`
--

LOCK TABLES `location_type` WRITE;
/*!40000 ALTER TABLE `location_type` DISABLE KEYS */;
INSERT INTO `location_type` (`location_type_uuid`, `location_type_code`, `location_type_name`, `last_modified_user`, `last_modified_dt`, `last_modified_action`) VALUES ('9469287f-c882-11e7-b411-000c29f66550','DISTRIBUTOR','Distributor','DEFAULT','2017-11-13 14:55:43','UPDATE'),('08a7d005-c886-11e7-b411-000c29f66550','HEADQUARTERS','Headquarters','DEFAULT','2017-11-13 15:19:25','INSERT'),('9469aeea-c882-11e7-b411-000c29f66550','RESELLER','Reseller','DEFAULT','2017-11-13 14:55:02','UPDATE'),('94696fc1-c882-11e7-b411-000c29f66550','STORE_FRONT','Store Front','DEFAULT','2017-11-13 14:55:02','UPDATE'),('fa93bdc6-c882-11e7-b411-000c29f66550','UNDEFINED','Undefined','DEFAULT','2017-11-13 15:19:25','UPDATE');
/*!40000 ALTER TABLE `location_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `paulsdevblog_hibernate_basic_example`.`location_type_BEFORE_INSERT` BEFORE INSERT ON `location_type` FOR EACH ROW
BEGIN
	-- ##################################################################################
	-- DECLAREs must be command first lines 
    -- ##################################################################################

	DECLARE var_trigger_current_dt DATETIME;
    DECLARE var_trigger_new_uuid VARCHAR(50); 
    
	-- ##################################################################################
    -- SET variables to unsafe functions as a work-around to make trigger replication safe
    -- ##################################################################################
    
    SET var_trigger_current_dt = ( UTC_TIMESTAMP() );
    SET var_trigger_new_uuid = ( SELECT UUID() );
    
	-- ##################################################################################
    -- SET last_modified values
    -- ##################################################################################
    
	SET NEW.last_modified_action = 'INSERT';
    SET NEW.last_modified_dt = var_trigger_current_dt ;
    
	-- ##################################################################################
    -- SET uuid key
    -- ##################################################################################
    SET NEW.location_type_uuid = IFNULL( NEW.location_type_uuid , var_trigger_new_uuid );
    
    IF ( NEW.location_type_uuid = '' ) THEN 
		SET NEW.location_type_uuid = var_trigger_new_uuid;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `paulsdevblog_hibernate_basic_example`.`location_type_BEFORE_UPDATE` BEFORE UPDATE ON `location_type` FOR EACH ROW
BEGIN
	-- ##################################################################################
	-- DECLAREs must be command first lines 
    -- ##################################################################################

	DECLARE var_trigger_current_dt DATETIME;
    DECLARE var_trigger_new_uuid VARCHAR(50); 
    
	-- ##################################################################################
    -- SET variables to unsafe functions as a work-around to make trigger replication safe
    -- ##################################################################################
    
    SET var_trigger_current_dt = ( UTC_TIMESTAMP() );
    SET var_trigger_new_uuid = ( SELECT UUID() );
    
	-- ##################################################################################
    -- SET last_modified values
    -- ##################################################################################
    
	SET NEW.last_modified_action = 'UPDATE';
    SET NEW.last_modified_dt = var_trigger_current_dt ;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'paulsdevblog_hibernate_basic_example'
--

--
-- Dumping routines for database 'paulsdevblog_hibernate_basic_example'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-13 17:11:15
